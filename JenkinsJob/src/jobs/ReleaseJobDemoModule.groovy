package jobs

//Parte General
mavenJob('Jenkins-Tutorial-Demo-ServicesApp-Release-DSL') {
    description 'Build job for Jenkins Tutorial for BuildParentProject'
    //numero maximo de ejecuciones para guardar
    logRotator {
        numToKeep 5
    }
    //ejecucion con parametros
    parameters {
        stringParam('releaseVersion', '',
                'The release version for the artifact. If you leave this empty, the current SNAPSHOT version ' +
                        'will be used with the "-SNAPSHOT" suffix removed (example: if the current version is ' +
                        '"1.0-SNAPSHOT", the release version will be "1.0").')

        stringParam('nextSnapshotVersion', '',
                'The snapshot version to be used after the release. If you leave this empty, the minor version ' +
                        'of the release will be incremented by one (example: if the release is "1.0", the next ' +
                        'snapshot version will be "1.1-SNAPSHOT").')
    }

    //configurar el origen el codigo fuente
    scm {
        git {
            remote {
                url('https://gitlab.com/JhonJC/moduleproject.git')
                credentials('gitlab_user')
            }
            branch('*/master')

            extensions {
                localBranch('master')
            }
        }
    }
    //pasos previos a la ejecucion
    preBuildSteps {
        systemGroovyCommand '''\
                import hudson.model.StringParameterValue
                import hudson.model.ParametersAction

                def env = build.getEnvironment(listener)
                String releaseVersion = env.get('releaseVersion')
                String nextSnapshotVersion = env.get('nextSnapshotVersion')

                println ">>>>EJECUTANDO GROOVY DSL"

                if (!releaseVersion) {                    
                    println ">>>>INICIANDO RELEASE THE ENTITYAPPLICATIONS"
                    String pomPathSon = build.workspace.toString() + '/EntityAplications/pom.xml';
                    def pomSon = new XmlSlurper().parse(new File(pomPathSon))
                    releaseVersion = pomSon.version.toString().replace('-SNAPSHOT', '')
                    println ">>>>>ENTITYAPPLICATIONS>>>>releaseVersion (calculated) = $releaseVersion"
                    def paramSon = new StringParameterValue('releaseVersion', releaseVersion)
                    build.replaceAction(new ParametersAction(paramSon))
               
                    println ">>>>INICIANDO RELEASE THE SERVICESAPPLICATIONS"
                    pomPath = build.workspace.toString() + '/ServicesAplications/pom.xml';
                    def pom = new XmlSlurper().parse(new File(pomPath))
                    releaseVersion = pom.version.toString().replace('-SNAPSHOT', '')
                    println ">>>>>>SERVICESAPPLICATIONS>>>releaseVersion (calculated) = $releaseVersion"
                    def param = new StringParameterValue('releaseVersion', releaseVersion)
                    build.replaceAction(new ParametersAction(param))
                }

                if (!nextSnapshotVersion) {
                    def tokens = releaseVersion.split('\\\\.')
                    nextSnapshotVersion = tokens[0] + '.' + (Integer.parseInt(tokens[1]) + 1) + '-SNAPSHOT\'
                    println "nextSnapshotVersion (calculated) = $nextSnapshotVersion"
                    def param1 = new StringParameterValue('releaseVersion', releaseVersion)
                    def param2 = new StringParameterValue('nextSnapshotVersion', nextSnapshotVersion)
                    build.replaceAction(new ParametersAction(param1, param2))
                }
                println "FINALIZANDO GROOVY DSL"  '''.stripIndent()

        //Ejecutar tare maven a nive superior
        maven {
            mavenInstallation 'latest'
            goals 'versions:set ' +
                    '-DnewVersion=${releaseVersion} ' +
                    '-DgenerateBackupPoms=false'
            rootPOM "ServicesAplications/pom.xml"
        }
        maven {
            mavenInstallation 'latest'
            goals 'versions:use-releases ' +
                    '-DgenerateBackupPoms=false ' +
                    '-DprocessDependencyManagement=true'
            rootPOM "ServicesAplications/pom.xml"
        }
        shell '''\
              echo '>>>>INICIANDO SHELL...'
              ls
              if find ServicesAplications/ -name 'pom.xml' | xargs grep -n "SNAPSHOT"; then
                echo 'SNAPSHOT versions not allowed in a release\'
                exit 1
              fi
              echo '>>>>FINALIZANDO SHELL...'
         '''.stripIndent()
    }
    //Compilacion o build
    rootPOM 'BuildParentProject/pom.xml'
    goals 'clean install -B -Penv-Production -DprofileFieldEnabled=true'


    //Pasos posteriores
    postBuildSteps('SUCCESS') {
        maven {
            mavenInstallation 'latest'
            goals 'scm:checkin ' +
                    '-Dmessage=">>>Release version ${project.artifactId}:${releaseVersion}" ' +
                    '-DdeveloperConnectionUrl=scm:git:https://wenikorejhon@gmail.com:1019090114@gitlab.com/JhonJC/moduleproject.git'
            rootPOM "ServicesAplications/pom.xml"
        }
        maven {
            mavenInstallation 'latest'
            goals 'scm:tag ' +
                    '-Dtag=${project.artifactId}-${releaseVersion} ' +
                    '-DdeveloperConnectionUrl=scm:git:https://wenikorejhon@gmail.com:1019090114@gitlab.com/JhonJC/moduleproject.git'
            rootPOM "ServicesAplications/pom.xml"
        }
        maven {
            mavenInstallation 'latest'
            goals 'versions:set ' +
                    '-DnewVersion=${nextSnapshotVersion} ' +
                    '-DgenerateBackupPoms=false'
            rootPOM "ServicesAplications/pom.xml"
        }
        maven {
            mavenInstallation 'latest'
            goals 'scm:checkin ' +
                    '-Dmessage="Switch to next snapshot version: ${project.artifactId}:${nextSnapshotVersion}" ' +
                    '-DdeveloperConnectionUrl=scm:git:https://wenikorejhon@gmail.com:1019090114@gitlab.com/JhonJC/moduleproject.git'
            rootPOM "ServicesAplications/pom.xml"
        }
        shell '''\
            echo '>>>>INICIANDO SHELL...PARA FINALIZAR'
            cd /var/lib/jenkins/workspace/Jenkins-Tutorial-Demo-ServicesApp-Release-DSL/ServicesAplications/target/
            ls
            cd /tmp/
            if [ ! -d PruebaDirectorio ]; then mkdir PruebaDirectorio; fi
            cd PruebaDirectorio
            RANDOM=$$
            echo $RANDOM
            touch $RANDOM.json
            echo '>>>>FINALIZANDO SHELL...PARA FINALIZAR'
         '''.stripIndent()

    }

}
