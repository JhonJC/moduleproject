package jobs

//Parte General
mavenJob('Jenkins-Tutorial-Demo-Basic-Build-DSL') {
    description 'Build job for Jenkins Tutorial for BuildParentProject'
    //numero maximo de ejecuciones para guardar
    logRotator {
        numToKeep 5
    }
    //ejecucion con parametros
    parameters {
        gitParam('Branch') {
            description 'The git branch to checkout'
            type 'BRANCH'
            defaultValue 'origin/master'
        }
    }
    //configurar el origen el codigo fuente
    scm {
        git {
            remote {
                url('https://gitlab.com/JhonJC/moduleproject.git')
                credentials('gitlab_user')
            }
            branch('$Branch')

            /*
            // se comenta esto permite  obtener alguns proyectos de mi repositorio
            //Add extensions 'SparseCheckoutPaths' and 'PathRestriction'
            def nodeBuilder = NodeBuilder.newInstance()
            def sparseCheckout = nodeBuilder.createNode('hudson.plugins.git.extensions.impl.SparseCheckoutPaths')
            sparseCheckout.appendNode('sparseCheckoutPaths')
                    .appendNode('hudson.plugins.git.extensions.impl.SparseCheckoutPath')
                    .appendNode('path', 'moduleproject/')

            sparseCheckoutPr1 = nodeBuilder.createNode('hudson.plugins.git.extensions.impl.SparseCheckoutPaths')
            sparseCheckoutPr1.appendNode('sparseCheckoutPaths')
                    .appendNode('hudson.plugins.git.extensions.impl.SparseCheckoutPath')
                    .appendNode('path', 'EntityAplications/')

            def sparseCheckoutPr2 = nodeBuilder.createNode('hudson.plugins.git.extensions.impl.SparseCheckoutPaths')
            sparseCheckoutPr2.appendNode('sparseCheckoutPaths')
                    .appendNode('hudson.plugins.git.extensions.impl.SparseCheckoutPath')
                    .appendNode('path', 'ServicesAplications/')

            def pathRestrictions = nodeBuilder.createNode('hudson.plugins.git.extensions.impl.PathRestriction')
            pathRestrictions.appendNode('includedRegions', 'BuildParentProject/.* EntityAplications/.* ServicesAplications/.*')

            def pathRestrictionsPro1 = nodeBuilder.createNode('hudson.plugins.git.extensions.impl.PathRestriction')
            pathRestrictions.appendNode('includedRegions', 'EntityAplications/.*')

            def pathRestrictionsPro2 = nodeBuilder.createNode('hudson.plugins.git.extensions.impl.PathRestriction')
            pathRestrictions.appendNode('includedRegions', 'ServicesAplications/.*')

            extensions {
                extensions << sparseCheckout
                //extensions << sparseCheckoutPr1
                //extensions << sparseCheckoutPr2
                extensions << pathRestrictions
                //extensions << pathRestrictionsPro1
                //extensions << pathRestrictionsPro2
            }*/
        }
    }
    //consulta de repositorio pot cada cuarto de hora
    triggers {
        scm 'H/15 * * * *'
        snapshotDependencies true
    }
    //Compilacion o build
    rootPOM 'BuildParentProject/pom.xml'
    goals 'clean install -B -Penv-Production -DprofileFieldEnabled=true'
}
