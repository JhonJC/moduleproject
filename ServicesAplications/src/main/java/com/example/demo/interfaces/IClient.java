package com.example.demo.interfaces;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.example.entities.UserInfo;

@Component("IClient")
public interface IClient {

	public abstract ResponseEntity<String> creteClient(UserInfo client);

}
