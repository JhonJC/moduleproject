package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.interfaces.IClient;
import com.example.entities.UserInfo;

@RestController
@RequestMapping(value = "${API}", produces = MediaType.APPLICATION_JSON_VALUE)

public class ImplClientsServices implements IClient {

	@PostMapping(value = "/createClient")
	@Override
	public ResponseEntity<String> creteClient(UserInfo client) {
		return new ResponseEntity<>(new String("User Create:" + client.getFirstName()), HttpStatus.OK);

	}

}
