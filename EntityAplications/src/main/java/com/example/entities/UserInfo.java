package com.example.entities;

public class UserInfo {

	private String firstName;
	private String secondName;
	private String lastname;
	private String email;

	public UserInfo(String firstName, String secondName, String lastname, String email) {
		super();
		this.firstName = firstName;
		this.secondName = secondName;
		this.lastname = lastname;
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
